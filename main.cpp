
#include "Tests.h"

//25 - NO ELEMENTS 42 - Array is to small for operation
//+ - слияние


const int NUMBER_OF_ANSWERS = 11;

const string welcome_msg = "WELCOME! What do you want to do?"
                           "\n1)Add student to selected deque"
                           "\n2)Add teacher to selected deque"
                           "\n3)Add person to selected deque"
                           "\n4)Select deque"
                           "\n5)Watch deque"
                           "\n6)Concatenate two deques"
                           "\n7)Sort deque by age of person"
                           "\n8)Get subdeque"
                           "\n9)Merge two deque"
                           "\n10)Find subdeque"
                           "\n11)Create new deque"
                           "\n0)Exit"
                           "\n>";

int selected_deque = 0; //0 - No selected deque
int number_of_deques = 0;

Deque<Person *> *array_of_deques;

void Start_deque_creation() {
    cout << "You have "
         << number_of_deques
         << " deques!\n";

    number_of_deques++;
    array_of_deques = (Deque<Person *> *) realloc(array_of_deques, sizeof(Deque<Person *>) * number_of_deques);
    Deque<Person *> new_deque = *(new Deque<Person *>());
    array_of_deques[number_of_deques - 1] = new_deque;

    cout << "New deque was created!" << endl;


}


void Add_deque(Deque<Person *> new_deque) {
    number_of_deques++;

    array_of_deques = (Deque<Person *> *) realloc(array_of_deques, sizeof(Deque<Person *>) * number_of_deques);
    array_of_deques[number_of_deques - 1] = new_deque;
}


int main() {
    cout << "Do you want to run tests? y/n" << endl;
    char c;
    cin >> c;
    if((c == 'y') || (c == 'Y')) {
        Tests t;
        t.Start_tests();
    }

    Start_deque_creation();
    cout << "You have selected " << 1 << " deque" << endl;
    selected_deque = 1;

    int ans;

    do {
        do {
            cout << welcome_msg;
            if (!(cin >> ans) || (cin.peek() != '\n') || ((ans < 0) || (ans > NUMBER_OF_ANSWERS))) {
                cin.clear();
                while (cin.get() != '\n');
                cout << "Incorrect input!" << endl;
                ans = -100;
            }

        } while ((ans < 0) || (ans > NUMBER_OF_ANSWERS));
        switch (ans) {
            case (0):
                return 0;
                break;
            case (1): {
                string first_name, middle_name, last_name, group;
                int age, passport_series, passport_number;

                cout << "Enter first name>";
                cin >> first_name;
                cout << endl;
                cout << "Enter middle name>";
                cin >> middle_name;
                cout << endl;
                cout << "Enter last name>";
                cin >> last_name;
                cout << endl;
                cout << "Enter group>";
                cin >> group;
                cout << endl;

                do {
                    cout << "Enter age>";
                    if (!(cin >> age) || (cin.peek() != '\n') || ((age < 0) || (age > 150))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        age = -100;
                    }

                } while ((age < 0) || (age > 150));
                cout << endl;

                do {
                    cout << "Enter passport [series] [number] if you want>";
                    if (!(cin >> passport_series >> passport_number) || (cin.peek() != '\n') ||
                        ((passport_series < 0) || (passport_number < 0))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        passport_number = -100;
                        passport_series = -100;
                    }

                } while ((passport_series < 0) || (passport_number < 0));

                cout << endl;

                Student *new_person = new Student(first_name, middle_name, last_name, age, group, passport_series,
                                                  passport_number);

                cout << *(new_person);

                cout << "Add it to the selected deque? >";

                char c1;
                cin >> c1;

                while (cin.get() != '\n');
                if ((c1 == 'y') | (c1 == 'Y')) {
                    cout << "Add it to the tail or head? t/h>";

                    char c2;
                    cin >> c2;
                    if ((c2 == 't') | (c2 == 'T')) {
                        array_of_deques[selected_deque - 1].Add_tail(new_person);
                    } else {
                        array_of_deques[selected_deque - 1].Add_head(new_person);
                    }
                } else {
                    cout << "Okay, then" << endl;
                    break;
                }

                break;
            }
            case (2): {
                string first_name, middle_name, last_name, department;
                int age, passport_series, passport_number;

                cout << "Enter first name>";
                cin >> first_name;
                cout << endl;
                cout << "Enter middle name>";
                cin >> middle_name;
                cout << endl;
                cout << "Enter last name>";
                cin >> last_name;
                cout << endl;
                cout << "Enter department>";
                cin >> department;
                cout << endl;

                do {
                    cout << "Enter age>";
                    if (!(cin >> age) || (cin.peek() != '\n') || ((age < 0) || (age > 150))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        age = -100;
                    }

                } while ((age < 0) || (age > 150));

                cout << endl;

                do {
                    cout << "Enter passport [series] [number] if you want>";
                    if (!(cin >> passport_series >> passport_number) || (cin.peek() != '\n') ||
                        ((passport_series < 0) || (passport_number < 0))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        passport_number = -100;
                        passport_series = -100;
                    }

                } while ((passport_series < 0) || (passport_number < 0));

                cout << endl;

                Teacher *new_person = new Teacher(first_name, middle_name, last_name, age, department, passport_series,
                                                  passport_number);

                cout << *(new_person);

                cout << "Add it to the selected deque? >";

                char c1;
                cin >> c1;

                while (cin.get() != '\n');
                if ((c1 == 'y') | (c1 == 'Y')) {
                    cout << "Add it to the tail or head? t/h>";

                    char c2;
                    cin >> c2;
                    if ((c2 == 't') | (c2 == 'T')) {
                        array_of_deques[selected_deque - 1].Add_tail(new_person);
                    } else {
                        array_of_deques[selected_deque - 1].Add_head(new_person);
                    }
                } else {
                    cout << "Okay, then" << endl;
                    break;
                }

                break;
            }
            case (3): {
                string first_name, middle_name, last_name, group, department;
                int age, passport_series, passport_number;

                cout << "Enter first name>";
                cin >> first_name;
                cout << endl;
                cout << "Enter middle name>";
                cin >> middle_name;
                cout << endl;
                cout << "Enter last name>";
                cin >> last_name;
                cout << endl;

                do {
                    cout << "Enter age>";
                    if (!(cin >> age) || (cin.peek() != '\n') || ((age < 0) || (age > 150))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        age = -100;
                    }

                } while ((age < 0) || (age > 150));

                cout << endl;

                do {
                    cout << "Enter passport [series] [number] if you want>";
                    if (!(cin >> passport_series >> passport_number) || (cin.peek() != '\n') ||
                        ((passport_series < 0) || (passport_number < 0))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        passport_number = -100;
                        passport_series = -100;
                    }

                } while ((passport_series < 0) || (passport_number < 0));

                cout << endl;

                Person *new_person = new Person(first_name, middle_name, last_name, age, passport_series,
                                                passport_number);

                cout << *(new_person);

                cout << "Add it to the selected deque? >";

                char c1;
                cin >> c1;

                while (cin.get() != '\n');
                if ((c1 == 'y') | (c1 == 'Y')) {
                    cout << "Add it to the tail or head? t/h>";

                    char c2;
                    cin >> c2;
                    if ((c2 == 't') | (c2 == 'T')) {
                        array_of_deques[selected_deque - 1].Add_tail(new_person);
                    } else {
                        array_of_deques[selected_deque - 1].Add_head(new_person);
                    }
                } else {
                    cout << "Okay, then" << endl;
                    break;
                }

                break;
            }
            case (4): {
                int c;
                do {
                    cout << "You have " << number_of_deques
                         << " deques\nWhat deque do you want to select?\nEnter number of deque to select of 0 to exit\n>"
                         << endl;
                    if (!(cin >> c) || (cin.peek() != '\n') || ((c < 0) || (c > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        c = -100;
                    }

                } while ((c < 0) || (c > number_of_deques));

                if (c == 0) {
                    cout << "Okay, then" << endl;
                    break;
                } else {
                    cout << "You have selected " << c << "deque"
                         << "Size of deque: " << array_of_deques[c - 1].Get_size() << endl;
                    selected_deque = c;

                }
                break;
            }
            case (5): {
                cout << "Do you want start from the head of from the tail? t/h>";
                char c2;
                cin >> c2;
                if ((c2 == 't') | (c2 == 'T')) {
                    array_of_deques[selected_deque - 1].Set_current(true);
                } else {
                    array_of_deques[selected_deque - 1].Set_current(false);
                }

                try {

                    array_of_deques[selected_deque - 1].Peek_current()->Show();
                } catch (int a) {
                    if (a == 25) {
                        cout << "Deque is empty!" << endl;
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }

                cout << "> - go to next"
                        "\n< - go to prev"
                        "\n Q - exit"
                        "\n H - Pop head"
                        "\n T - Pop tail>";
                do {
                    cin >> c2;
                    if ((c2 == '>') && (!array_of_deques[selected_deque - 1].Head())) {
                        array_of_deques[selected_deque - 1].Next();
                        array_of_deques[selected_deque - 1].Peek_current()->Show();
                    } else if ((c2 == '<') && (!array_of_deques[selected_deque - 1].Tail())) {
                        array_of_deques[selected_deque - 1].Prev();
                        array_of_deques[selected_deque - 1].Peek_current()->Show();
                    } else if ((c2 == 'q') || (c2 == 'Q')) {
                        break;
                    } else if ((c2 == 'h') || (c2 == 'H')) {
                        if (array_of_deques[selected_deque - 1].Head() &&
                            array_of_deques[selected_deque - 1].Get_size() != 1) {
                            array_of_deques[selected_deque - 1].Prev();
                            array_of_deques[selected_deque - 1].Peek_current()->Show();
                        }
                        array_of_deques[selected_deque - 1].Pop_head();
                    } else if ((c2 == 't') || (c2 == 'T')) {
                        if (array_of_deques[selected_deque - 1].Tail() &&
                            array_of_deques[selected_deque - 1].Get_size() != 1) {
                            array_of_deques[selected_deque - 1].Next();
                            array_of_deques[selected_deque - 1].Peek_current()->Show();
                        }
                        array_of_deques[selected_deque - 1].Pop_tail();
                    } else {
                        cout << "Incorrect" << endl;
                    }

                    if (array_of_deques[selected_deque - 1].Is_empty()) {
                        cout << "Deque is empty now!" << endl;
                        break;
                    }

                } while (true);
                break;
            }
            case (6): {

                int first, second;

                do {
                    cout << "You have " << number_of_deques
                         << " deques\nWhich deuqes you want to concatenate?"
                         << "[first deque] [second deque] >";
                    if (!(cin >> first) || !(cin >> second) || (cin.peek() != '\n') ||
                        ((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        first = -100;
                        second = -100;
                    }
                } while (((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques)));

                Deque<Person *> new_deque;

                try {
                    new_deque = array_of_deques[first - 1] + array_of_deques[second - 1];
                } catch (int error_code) {
                    if (error_code == 25) {
                        cout << "Empty deque was passed!" << endl;
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }
                char c;

                cout << "Do you want to save result?"
                     << "\nYou can watch it later y/n >";
                cin >> c;

                if ((c == 'y') || (c == 'Y')) {
                    Add_deque(new_deque);
                } else {
                    cout << "\nOk" << endl;
                }


                break;
            }
            case (7): {

                int c;

                do {
                    cout << "You have " << number_of_deques
                         << " deques\nWhich deuqe you want to sort?"
                         << "[deque number] >";
                    if (!(cin >> c) || (cin.peek() != '\n') || ((c < 0) || (c > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        c = -100;
                    }

                } while ((c < 0) || (c > number_of_deques));

                try {
                    array_of_deques[c - 1].Sort();
                } catch (int error_code) {
                    if (error_code == 25) {
                        cout << "Empty deque was passed!" << endl;
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }
                cout << "Sort was completed!" << endl;

                break;
            }
            case (8): {
                int index, start, end;

                do {
                    cout << "You have " << number_of_deques
                         << " deques\nFrom which deque do you want to get subdeque?"
                         << "[deque number] [first index] [last index]  >";
                    if (!(cin >> index) || !(cin >> start) || !(cin >> end) || (cin.peek() != '\n') ||
                        ((index < 0) || (index > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        index = -100;
                    }

                } while ((index < 0) || (index > number_of_deques));

                Deque<Person *> new_deque;

                try {
                    new_deque = array_of_deques[index - 1].Get_subdeque(start, end);
                } catch (int error_code) {
                    if (error_code == 25) {
                        cout << "Empty deque was passed!" << endl;
                        break;
                    } else if (error_code == 42) {
                        cout << "End index > size of deque";
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }
                char c;

                cout << "Do you want to save result?"
                     << "\nYou can watch it later y/n >";
                cin >> c;

                if ((c == 'y') || (c == 'Y')) {
                    Add_deque(new_deque);
                } else {
                    cout << "\nOk" << endl;
                }
                break;
            }
            case (9): {
                int first, second;

                do {
                    cout << "You have " << number_of_deques
                         << " deques\nWhich deuqes you want to merge?"
                         << "[first deque] [second deque] >";
                    if (!(cin >> first) || !(cin >> second) || (cin.peek() != '\n') ||
                        ((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        first = -100;
                        second = -100;
                    }
                } while (((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques)));

                Deque<Person *> new_deque;

                try {
                    new_deque = array_of_deques[first - 1] * array_of_deques[second - 1];
                } catch (int error_code) {
                    if (error_code == 25) {
                        cout << "Empty deque was passed!" << endl;
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }

                char c;

                cout << "Do you want to save result?"
                     << "\nYou can watch it later y/n >";
                cin >> c;

                if ((c == 'y') || (c == 'Y')) {
                    Add_deque(new_deque);
                } else {
                    cout << "\nOk" << endl;
                }
                break;
            }
            case (10): {
                int first, second;

                do {
                    cout << "You have " << number_of_deques
                         << " deques\nEnter number of deque and subdeque?"
                         << "[deque] [subdeque] >";
                    if (!(cin >> first) || !(cin >> second) || (cin.peek() != '\n') ||
                        ((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques))) {
                        cin.clear();
                        while (cin.get() != '\n');
                        cout << "Incorrect input!" << endl;
                        first = -100;
                        second = -100;
                    }
                } while (((first < 0) || (first > number_of_deques)) || ((second < 0) || (second > number_of_deques)));

                int index;
                try {
                    index = array_of_deques[first - 1].Find(array_of_deques[second - 1]);
                } catch (int error_code) {
                    if (error_code == 25) {
                        cout << "Empty deque was passed!" << endl;
                        break;
                    } else {
                        cout << "Unexpected error!" << endl;
                        return 0;
                    }
                }

                if (index == -1) {
                    cout << "Subdeque was not found!";
                } else {
                    cout << "Subdeque was found from " << index << " element";
                }
                break;
            }
            case (11):
                Start_deque_creation();
                break;

        }
    } while (true);

    return 0;
}

