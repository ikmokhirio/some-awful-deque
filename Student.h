//
// Created by ikmokhirio on 19.04.2019.
//
#include "Person.h"


class Student : public Person {
private:
    string group;
public:
    Student(string first_name, string middle_name, string last_name, int age, string group, int passport_series = 0,
            int passport_number = 0);

    friend std::ostream &operator<<(std::ostream &, Student &student);

    void Show();

    string Get_group();

    void Set_group(string group);
};
