//
// Created by ikmokhirio on 24.04.2019.
//
#include <iostream>
#include "Teacher.h"

using namespace std;

Teacher::Teacher(string first_name, string middle_name, string last_name, int age, string department,
                 int passport_series, int passport_number) : Person(first_name, middle_name, last_name, age,
                                                                    passport_series, passport_number) {
    this->department = department;
}

void Teacher::Show() {
    cout << *(this);
}

ostream &operator<<(ostream &out, Teacher &teacher) {
    out << "Teacher\nFirst name: " << teacher.Get_first_name()
        << "\nMiddle name: " << teacher.Get_middle_name()
        << "\nLast name: " << teacher.Get_last_name()
        << "\nAge: " << teacher.Get_age()
        << "\nDepartment: " << teacher.Get_department()
        << endl;

    if ((teacher.Get_passport()->Get_number() != 0) && (teacher.Get_passport()->Get_series() != 0)) {
        out << "Passport series: " << teacher.Get_passport()->Get_series()
            << "  Passport number: " << teacher.Get_passport()->Get_number() << endl;
    }
    return out;
}

void Teacher::Set_department(string department) {
    this->department = department;
}

string Teacher::Get_department() {
    return department;
}