//
// Created by ikmokhirio on 18.04.2019.
//

template<class TYPE>
class Element {
private:
    Element *next;
    Element *prev;
    TYPE data;
public:
    Element(TYPE data) {

        this->data = data;
    }

    void Set_next(Element<TYPE> *next) {
        this->next = next;
    }


    void Set_prev(Element<TYPE> *prev) {
        this->prev = prev;
    }

    Element *Get_next() {
        return next;
    }

    Element *Get_prev() {
        return prev;
    }

    TYPE Get_data() {
        return (data);
    }

    void Set_data(TYPE data) {
        this->data = data;
    }
};