//
// Created by ikmokhirio on 24.04.2019.
//

#include "Passport.h"

Passport::Passport(int series, int number) {
    this->series = series;
    this->number = number;
}

int Passport::Get_number() {
    return number;
}

int Passport::Get_series() {
    return series;
}

void Passport::Set_number(int number) {
    this->number = number;
}

void Passport::Set_series(int series) {
    this->series = series;
}
