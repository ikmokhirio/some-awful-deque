//
// Created by ikmokhirio on 19.04.2019.
//
#include <iostream>
#include "Student.h"

using namespace std;

Student::Student(string first_name, string middle_name, string last_name, int age, string group, int passport_series,
                 int passport_number) : Person(first_name, middle_name, last_name, age, passport_series,
                                               passport_number) {
    this->group = group;

}

ostream &operator<<(ostream &out, Student &student) {
    out << "Student\nFirst name: " << student.Get_first_name()
        << "\nMiddle name: " << student.Get_middle_name()
        << "\nLast name: " << student.Get_last_name()
        << "\nAge: " << student.Get_age()
        << "\nGroup: " << student.Get_group() << endl;
    if ((student.Get_passport()->Get_number() != 0) && (student.Get_passport()->Get_series() != 0)) {
        out << "Passport series: " << student.Get_passport()->Get_series()
            << "  Passport number: " << student.Get_passport()->Get_number() << endl;
    }
    return out;
}

void Student::Show() {
    cout << (*this);
}

void Student::Set_group(string group) {
    this->group = group;
}

string Student::Get_group() {
    return group;
}
