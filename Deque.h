#include "Element.h"
#include "Person.h"

//
// Created by ikmokhirio on 18.04.2019.
//

#include <iostream>
//25 - One of deque is empty
//42 - Subdeque is bigger then deque
using namespace std;
// TAIL ELEMENT ELEMENT ELEMENT HEAD
// SORT STARTS FROM TAIL

template<class TYPE>
class Deque {
private:
    Element<TYPE> *head;
    Element<TYPE> *tail;
    Element<TYPE> *current; //Need to get element with number i
    int size;
public:
    //Standard methods for work with deque
    Deque(TYPE data) {
        Element<TYPE> *new_elem = new Element<TYPE>(data);
        head = new_elem;
        head->Set_prev(new_elem);
        tail = new_elem;
        tail->Set_next(new_elem);
        size = 1;
    }

    Deque() {
        size = 0;
    }

    void Add_head(TYPE data) {
        if (!Is_empty()) {
            Element<TYPE> *new_elem = new Element<TYPE>(data);
            head->Set_next(new_elem);
            new_elem->Set_prev(head);
            head = new_elem;
            size++;
        } else {
            Element<TYPE> *new_elem = new Element<TYPE>(data);
            head = new_elem;
            head->Set_prev(new_elem);
            tail = new_elem;
            tail->Set_next(new_elem);
            size = 1;
        }
    }

    void Add_tail(TYPE data) {
        if (!Is_empty()) {
            Element<TYPE> *new_elem = new Element<TYPE>(data);
            tail->Set_prev(new_elem);
            new_elem->Set_next(tail);
            tail = new_elem;
            size++;
        } else {
            Element<TYPE> *new_elem = new Element<TYPE>(data);
            head = new_elem;
            head->Set_prev(new_elem);
            tail = new_elem;
            tail->Set_next(new_elem);
            size = 1;
        }
    }

    TYPE Pop_head() {
        if (!Is_empty()) {
            if (size > 1) {
                TYPE return_element = head->Get_data();
                head = head->Get_prev();
                delete head->Get_next();
                head->Set_next(nullptr);
                size--;
                return return_element;
            } else {
                TYPE return_element = head->Get_data();
                delete head;
                size--;
                return return_element;
            }
        } else {
            throw 25;
        }
    }

    TYPE Pop_tail() {
        if (!Is_empty()) {
            if (size > 1) {
                TYPE return_element = tail->Get_data();
                tail = tail->Get_next();
                delete tail->Get_prev();
                tail->Set_prev(nullptr);
                size--;
                return return_element;
            } else {
                TYPE return_element = tail->Get_data();
                delete tail;
                size--;
                return return_element;
            }
        } else {
            throw 25;
        }
    }

    TYPE Peek_current() {
        if (!Is_empty()) {
            return current->Get_data();
        } else {
            throw 25;
        }
    }

    void Next() {
        current = current->Get_next();
    }

    void Prev() {
        current = current->Get_prev();
    }

    void Set_current(bool is_tail) {
        if (is_tail) {
            current = tail;
        } else {
            current = head;
        }
    }

    Element<TYPE> *Get_tail() {
        return tail;
    }

    Element<TYPE> *Get_head() {
        return head;
    }


    bool Head() {
        return current == head;
    }

    bool Tail() {
        return current == tail;
    }

    TYPE Peek_head() {
        if (!Is_empty()) {
            return head->Get_data();
        }
    }

    TYPE Peek_tail() {
        if (!Is_empty()) {
            return tail->Get_data();
        }
    }


    int Get_size() {
        return size;
    }

    bool Is_empty() {
        return (size == 0);
    }

    //End of standard methods


    //Concatenation returns deque that contains
    // first -> elements -> then -> second -> elements
    friend Deque operator+(Deque first, Deque second) {
        Deque<TYPE> new_deque;

        if (first.Is_empty() || second.Is_empty()) {
            throw 25;
        }

        first.Set_current(true);
        second.Set_current(true);

        new_deque.Add_head(first.Peek_current());
        if (first.Get_size() != 1) {
            do {

                first.Next();
                new_deque.Add_head(first.Peek_current());
            } while (!first.Head());
        }
        new_deque.Add_head(second.Peek_current());
        if (second.Get_size() != 1) {
            do {

                second.Next();
                new_deque.Add_head(second.Peek_current());
            } while (!second.Head());
        }
        //cout << "SIZE" << new_deque.Get_size() << endl;
        return new_deque;//Concatenate
    }


//Sort for deque
//Using buuble sort
    void Sort() {
        Element<TYPE> *tmp = tail;
        for (int i = 0; i < size; i++) {
            current = head;
            for (int j = size - 1; j > i; j--) {

                if (*(current->Get_prev()->Get_data()) > *(current->Get_data())) {

                    tmp = current->Get_prev();

                    if (tmp == tail) {
                        current->Set_prev(nullptr);
                        tmp->Set_next(current->Get_next());

                        tmp->Get_next()->Set_prev(tmp);
                        tmp->Set_prev(current);

                        current->Set_next(tmp);
                        tail = current;
                    } else if (current == head) {
                        current->Set_prev(tmp->Get_prev());
                        current->Get_prev()->Set_next(current);


                        tmp->Set_prev(current);
                        current->Set_next(tmp);

                        tmp->Set_next(nullptr);
                        head = tmp;
                    } else {
                        current->Set_prev(current->Get_prev()->Get_prev());
                        current->Get_prev()->Set_next(current);

                        tmp->Set_prev(current);
                        tmp->Set_next(current->Get_next());

                        tmp->Get_next()->Set_prev(tmp);
                        current->Set_next(tmp);
                    }

                } else {
                    current = current->Get_prev();
                }
            }
        }
    }

//Returns subdeque starting from [start] element to [end] element
    Deque<TYPE> Get_subdeque(int start, int end) {

        if (Is_empty()) {
            throw 25; //Empty deque was passed
        }

        if (end > size) {
            throw 42; //Invalid parameters
        }

       Set_current(true);
        while (start > 0) {
            Next();
            start--;
            end--;
        }
        Deque<TYPE> new_deque(Peek_current());
        while (end > 0) {
            Next();
            //!cout << end << endl;
            new_deque.Add_head(Peek_current());
            end--;
        }

        return new_deque;
    }//Get subdeque from deque

//Methods that using function for every element of deque
    Deque<TYPE> Where(bool (*Function)(TYPE)) {

        if (Is_empty()) {
            throw 25;
        }

        Deque<TYPE> new_deque;

        Set_current(true);

        if (Function(current->Get_data())) {
            new_deque.Add_head(current->Get_data());
        }

        while (!Head()) {
            Next();
            if (Function(current->Get_data())) {
                new_deque.Add_head(current->Get_data());
            }
        }

        return new_deque;
    }

//Function that recursively using function for every element of deque
//Returns deque with only one element
    Deque<TYPE> Reduce(TYPE (*Function)(TYPE, TYPE), TYPE constant) {

        if (Is_empty()) {
            throw 25;
        }


        Deque<TYPE> new_deque;

        Set_current(true);

        TYPE result = Function(current->Get_data(), constant);

        while (!Head()) {
            Next();
            result = Function(current->Get_data(), result);
        }

        new_deque.Add_head(result);

        return new_deque;
    }

    Deque<TYPE> Map(TYPE (*Function)(TYPE)) {

        if (Is_empty()) {
            throw 25;
        }


        Deque<TYPE> new_deque;

        Set_current(true);

        new_deque.Add_head(Function(current->Get_data()));

        while (!Head()) {
            Next();
            new_deque.Add_head(Function(current->Get_data()));
        }

        return new_deque;
    }

//Merging two deques
//Places elements [first]1 -> [second]1 -> [first]2 -> [second]2 -> ...
    friend Deque operator*(Deque first, Deque second) {

        if (first.Is_empty() || second.Is_empty()) {
            throw 25;
        }


        Deque<TYPE> new_deque;

        first.Set_current(true);
        second.Set_current(true);

        new_deque.Add_head(first.Peek_current());
        new_deque.Add_head(second.Peek_current());

        while ((!first.Head()) && (!second.Head())) {

            first.Next();
            new_deque.Add_head(first.Peek_current());
            second.Next();
            new_deque.Add_head(second.Peek_current());
        }


        if (first.Head() && second.Head()) {
            return new_deque;
        } else if (first.Head()) {
            while (!second.Head()) {
                second.Next();
                new_deque.Add_head(second.Peek_current());
            }
        } else if (second.Head()) {
            while (!first.Head()) {
                first.Next();
                new_deque.Add_tail(first.Peek_current());
            }
        }

        return new_deque;
    } //Merge

//Search for subdeque in deque
//Returns index of first entry
    int Find(Deque subdeque) {

        if (Is_empty()) {
            throw 25;
        }


        if (subdeque.Get_size() > size) {
            return -1;
        }

        int count_of_steps = 0;
        int num = 0;


        while (count_of_steps + subdeque.Get_size() <= size) {
            Set_current(true);
            subdeque.Set_current(true);
            for (int i = 0; i < count_of_steps; i++) {
                Next();
            }

            do {
                if (*(current->Get_data()) == *(subdeque.Peek_current())) {
                    if (subdeque.Head()) {
                        return count_of_steps;
                    }
                    num++;
                    subdeque.Next();
                    Next();
                } else {
                    break;
                }
            } while (!subdeque.Head());
            if (*(current->Get_data()) == *(subdeque.Peek_current())) {
                num++;
            }


            if (num == subdeque.Get_size()) {
                return count_of_steps;
            } else {
                num = 0;
            }
            count_of_steps++;

        }

        return -1;
    } //Simple search


//Divide deque into two with function
//First deque contains only elements which gives true in function
//Second deque contains only elements which gives false in function
    Deque<TYPE> *Division(bool (*Function)(TYPE)) {

        if (Is_empty()) {
            throw 25;
        }


        Deque<TYPE> *array_of_deque = (Deque<TYPE> *) malloc(sizeof(Deque<TYPE>) * 2);

        array_of_deque[0] = *(new Deque<TYPE>);
        array_of_deque[1] = *(new Deque<TYPE>);

        Set_current(true);

        if (Function(current->Get_data())) {
            array_of_deque[0].Add_head(current->Get_data());
        } else {
            array_of_deque[1].Add_head(current->Get_data());
        }

        while (!Head()) {
            Next();
            if (Function(current->Get_data())) {
                array_of_deque[0].Add_head(current->Get_data());
            } else {
                array_of_deque[1].Add_head(current->Get_data());
            }
        }

        return array_of_deque;
    }


};
