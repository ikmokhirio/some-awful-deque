//
// Created by ikmokhirio on 18.04.2019.
//

#include <string>
#include "Passport.h"

using namespace std;

#ifndef STRUCTURES_PERSON_H
#define STRUCTURES_PERSON_H


class Person {
private:
    int age;
    string first_name;
    string middle_name;
    string last_name;
    Passport *pass = new Passport(0, 0);
public:
    Person(string first_name, string middle_name, string last_name, int age, int passport_series = 0,
           int passport_number = 0);

    void Set_age(int age);

    void Set_first_name(string first_name);

    void Set_middle_name(string middle_name);

    void Set_last_name(string last_name);

    int Get_age();

    string Get_first_name();

    string Get_middle_name();

    string Get_last_name();

    virtual void Show();

    Passport *Get_passport();

    friend std::ostream &operator<<(std::ostream &out, Person &person);

    friend bool operator>(Person first, Person second);

    friend bool operator>=(Person first, Person second);

    friend bool operator<(Person first, Person second);

    friend bool operator<=(Person first, Person second);

    friend bool operator==(Person first, Person second);

    friend bool operator!=(Person first, Person second);
};


#endif //STRUCTURES_PERSON_H
