#include <iostream>
#include "Person.h"
#include "Deque.h"
#include "Student.h"
#include "Teacher.h"
//
// Created by ikmokhirio on 26.04.2019.
//


const int NUMBER_OF_TESTS = 8;

class Tests {
private:
    bool results[NUMBER_OF_TESTS];
    int current_test;
public:

    Tests();

    void Sort_test();

    void Map_test();

    void Where_test();

    void Reduce_test();

    void Concatenation_test();

    void Division_test();

    void Get_subdeque_test();

    void Find_test();

    void Merge_sort();

    void Show_final_result();

    void Show_deque(Deque<Person *> deq);

    void Start_tests();
};


