//
// Created by ikmokhirio on 26.04.2019.
//

#include "Tests.h"

Tests::Tests() {
    current_test = 0;

}

void Tests::Start_tests() {
    Sort_test();

    Map_test();

    Where_test();

    Reduce_test();

    Concatenation_test();

    Division_test();

    Get_subdeque_test();

    Find_test();

    Merge_sort();

    Show_final_result();

}

bool Check_expected(Deque<Person *> first, Deque<Person *> second) {
    if (first.Get_size() != second.Get_size()) {
        return false;
    }

    first.Set_current(true);
    second.Set_current(true);

    if ((first.Peek_current()->Get_age() != second.Peek_current()->Get_age())
        || (first.Peek_current()->Get_first_name() != second.Peek_current()->Get_first_name())
        || (first.Peek_current()->Get_middle_name() != second.Peek_current()->Get_middle_name())
        || (first.Peek_current()->Get_last_name() != second.Peek_current()->Get_last_name())) {

        return false;
    }

    while (!first.Head()) {
        first.Next();
        second.Next();

        if ((first.Peek_current()->Get_age() != second.Peek_current()->Get_age())
            || (first.Peek_current()->Get_first_name() != second.Peek_current()->Get_first_name())
            || (first.Peek_current()->Get_middle_name() != second.Peek_current()->Get_middle_name())
            || (first.Peek_current()->Get_last_name() != second.Peek_current()->Get_last_name())) {

            return false;

        }
    }

    return true;
}

void Tests::Show_deque(Deque<Person *> deq) {

    cout << endl;
    deq.Set_current(true);
    deq.Peek_current()->Show();
    cout << endl;
    while (!deq.Head()) {
        deq.Next();
        deq.Peek_current()->Show();
        cout << endl;
    }
    cout << "\n\n";
}

void Tests::Sort_test() {
    Deque<Person *> test_deque;

    cout << "Deque sort test" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Deque for test: " << endl;
    Show_deque(test_deque);

    test_deque.Sort();

    Deque<Person *> expected_deque;
    expected_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    expected_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    expected_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Expected result: " << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(test_deque);

    if (Check_expected(test_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Sort test was passed!" << endl;
    } else {
        cout << "Sort test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;

}


Person *Increase_age(Person *person) {

    person->Set_age((person->Get_age()) + 1);
    return person;
}

void Tests::Map_test() {

    Deque<Person *> test_deque;

    cout << "Map test"
         << "\nFunction for map increasing age for all person by 1" << endl;


    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Deque for test: " << endl;
    Show_deque(test_deque);

    Deque<Person *> expected_deque;


    test_deque.Map(Increase_age);

    expected_deque.Add_head(new Person("t", "z", "a", 67, 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 26, 0, 0));
    expected_deque.Add_head(new Student("t", "z", "a", 126, "A", 0, 0));
    expected_deque.Add_head(new Teacher("t", "z", "a", 43, "B", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 229, 0, 0));

    cout << "Expected result: " << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(test_deque);

    if (Check_expected(test_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Map test was passed!" << endl;
    } else {
        cout << "Map test was failed!" << endl;
        results[current_test] = false;
    }

    current_test++;

}

bool Where_function(Person *a) {
    return a->Get_age() > 66;
}

void Tests::Where_test() {
    Deque<Person *> test_deque;

    cout << "Where test"
         << "\nFunction for where returns true if person's age > 66" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    Deque<Person *> new_deque = test_deque.Where(Where_function);

    cout << "Deque for test:" << endl;
    Show_deque(test_deque);

    Deque<Person *> expected_deque;

    expected_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Expected result: " << endl;
    Show_deque(expected_deque);

    cout << "Result: " << endl;
    Show_deque(new_deque);

    if (Check_expected(new_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Where test was passed!" << endl;
    } else {
        cout << "Where test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;

}

Person *Get_oldest(Person *first, Person *second) {
    if (*(first) >= *(second)) {
        return first;
    } else {
        return second;
    }
}

void Tests::Reduce_test() {

    Deque<Person *> test_deque;

    cout << "Reduce test"
         << "\nFunction returns person with the biggest age" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    Person *constant = new Person("1", "2", "3", 0, 0, 0);
    Deque<Person *> new_deque = test_deque.Reduce(Get_oldest, constant);

    cout << "Deque for test:" << endl;
    Show_deque(test_deque);

    Deque<Person *> expected_deque;

    expected_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Expected result: " << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(new_deque);

    if (Check_expected(new_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Reduce test was passed!" << endl;
    } else {
        cout << "Reduce test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;

}

void Tests::Concatenation_test() {
    Deque<Person *> test_deque;

    cout << "Concatenation test" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));

    Deque<Person *> test_deque2;

    test_deque2.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque2.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque2.Add_head(new Person("t", "z", "a", 228, 0, 0));

    Deque<Person *> new_deque = test_deque + test_deque2;

    cout << "First deque for test:" << endl;
    Show_deque(test_deque);

    cout << "Second deque for test:" << endl;
    Show_deque(test_deque2);

    Deque<Person *> expected_deque;

    expected_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    expected_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "Expected result:" << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(new_deque);

    if (Check_expected(new_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Concatenation test was passed!" << endl;
    } else {
        cout << "Concatenation test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;
}

void Tests::Division_test() {
    Deque<Person *> test_deque;

    cout << "Division test"
         << "\nFunction for division it is same function as in where test" << endl;


    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    Deque<Person *> *array_of_deque = test_deque.Division(Where_function);

    cout << "Deque for test:" << endl;
    Show_deque(test_deque);

    Deque<Person *> expected_deque1;
    Deque<Person *> expected_deque2;


    expected_deque2.Add_head(new Person("t", "z", "a", 66, 0, 0));
    expected_deque2.Add_head(new Person("t", "z", "a", 25, 0, 0));
    expected_deque1.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque2.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    expected_deque1.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "First expected deque:" << endl;
    Show_deque(expected_deque1);

    cout << "Second expected deque:" << endl;
    Show_deque(expected_deque2);

    cout << "First result deque:" << endl;
    Show_deque(array_of_deque[0]);

    cout << "Second result deque:" << endl;
    Show_deque(array_of_deque[1]);

    if (Check_expected(array_of_deque[0], expected_deque1) && Check_expected(array_of_deque[1], expected_deque2)) {
        results[current_test] = true;
        cout << "Division test was passed!" << endl;
    } else {
        Show_deque(array_of_deque[0]);
        cout << "SECOND" << endl;
        Show_deque(array_of_deque[1]);
        cout << "Division test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;
}

void Tests::Get_subdeque_test() {
    Deque<Person *> test_deque;

    cout << "Getting subdeuque test" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    test_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));


    Deque<Person *> new_deque = test_deque.Get_subdeque(1, 3);

    cout << "Deque for test:" << endl;
    Show_deque(test_deque);

    Deque<Person *> expected_deque;

    expected_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    expected_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));

    cout << "Expected result:" << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(new_deque);

    if (Check_expected(new_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Subdeque test was passed!" << endl;
    } else {
        cout << "Subdeque test was passed!" << endl;
        results[current_test] = false;
    }
    current_test++;

}

void Tests::Find_test() {
    Deque<Person *> test_deque;

    cout << "Finding subdeque test" << endl;

    test_deque.Add_head(new Person("t", "z", "a", 42, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    //42 228

    cout << "Subdeque for test:" << endl;
    Show_deque(test_deque);

    Deque<Person *> test_deque2;

    test_deque2.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque2.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque2.Add_head(new Person("t", "z", "a", 228, 0, 0));
    //125 42 228

    cout << "Deque for test:" << endl;
    Show_deque(test_deque2);

    int answer = test_deque2.Find(test_deque);

    int expected_answer = 1;

    cout << "Expected result: " << "deque stats from index " << expected_answer << endl;
    cout << "Result: " << "deque stats from index " << answer << endl;


    if (answer == expected_answer) {
        cout << "Find test was passed!" << endl;
        results[current_test] = true;
    } else {
        cout << "Find test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;
}

void Tests::Merge_sort() {
    cout << "Merge sort" << endl;

    Deque<Person *> test_deque;

    test_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    test_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));

    Deque<Person *> test_deque2;

    test_deque2.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    test_deque2.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    test_deque2.Add_head(new Person("t", "z", "a", 228, 0, 0));

    cout << "First deque for test:" << endl;
    Show_deque(test_deque);

    cout << "Second deque for test:" << endl;
    Show_deque(test_deque2);

    Deque<Person *> new_deque = test_deque * test_deque2;

    //Show_deque(new_deque);
    //cout << "TESTEST\n\n";

    Deque<Person *> expected_deque;

    expected_deque.Add_head(new Person("t", "z", "a", 66, 0, 0));
    expected_deque.Add_head(new Student("t", "z", "a", 125, "A", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 25, 0, 0));
    expected_deque.Add_head(new Teacher("t", "z", "a", 42, "B", 0, 0));
    expected_deque.Add_head(new Person("t", "z", "a", 228, 0, 0));

    //Show_deque(expected_deque);

    cout << "Expected result:" << endl;
    Show_deque(expected_deque);

    cout << "Result:" << endl;
    Show_deque(new_deque);

    if (Check_expected(new_deque, expected_deque)) {
        results[current_test] = true;
        cout << "Merge test was passed!" << endl;
    } else {
        cout << "Merge test was failed!" << endl;
        results[current_test] = false;
    }
    current_test++;
}

void Tests::Show_final_result() {
    int good = 0;
    int bad = 0;
    for (int i = 0; i < NUMBER_OF_TESTS; i++) {
        if (results[i]) {
            good++;
        } else {
            bad++;
        }
    }

    if (good == 8) {
        cout << "All tests were passed!" << endl;
    } else {
        cout << good << " tests were passed" << endl;
        cout << bad << " tests were failed" << endl;
    }
}