//
// Created by ikmokhirio on 24.04.2019.
//
#include "Person.h"


class Teacher : public Person {
private:
    string department;
public:
    Teacher(string first_name, string middle_name, string last_name, int age, string department,
            int passport_series = 0, int passport_number = 0);

    friend std::ostream &operator<<(std::ostream &, Teacher &teacher);

    void Show();

    string Get_department();

    void Set_department(string department);
};


