//
// Created by ikmokhirio on 24.04.2019.
//

#ifndef STRUCTURES_PASSPORT_H
#define STRUCTURES_PASSPORT_H


class Passport {
private:
    int series;
    int number;
public:
    Passport(int series, int number);

    int Get_series();

    int Get_number();

    void Set_series(int series);

    void Set_number(int number);
};


#endif //STRUCTURES_PASSPORT_H
